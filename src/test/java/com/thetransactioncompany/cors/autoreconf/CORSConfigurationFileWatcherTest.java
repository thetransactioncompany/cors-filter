package com.thetransactioncompany.cors.autoreconf;


import com.thetransactioncompany.cors.CORSConfigurationLoader;
import com.thetransactioncompany.cors.MockFilterConfig;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Tests the CORS configuration file watcher.
 */
public class CORSConfigurationFileWatcherTest {


	@BeforeEach
	public void setUp()
		throws Exception {

		Properties properties = new Properties();
		properties.setProperty("cors.allowOrigin", "https://www.example.org");

		OutputStream os = Files.newOutputStream(new File("test.properties").toPath());
		properties.store(os, null);
	}


	@Test
	public void testWatchParameterName() {

		assertEquals("cors.configFilePollInterval", CORSConfigurationFileWatcher.POLL_INTERVAL_PARAM_NAME);
	}


	@Test
	public void testDefaultWatchIntervalConstant() {

		assertEquals(20, CORSConfigurationFileWatcher.DEFAULT_POLL_INTERVAL_SECONDS);
	}


	@Test
	public void testDefaultWatchInterval() {

		MockFilterConfig filterConfig = new MockFilterConfig();
		filterConfig.setInitParameter(CORSConfigurationLoader.CONFIG_FILE_PARAM_NAME, "test.properties");

		CORSConfigurationFileWatcher watcher = new CORSConfigurationFileWatcher(filterConfig);

		assertEquals(CORSConfigurationFileWatcher.DEFAULT_POLL_INTERVAL_SECONDS, watcher.getPollIntervalSeconds());
	}


	@Test
	public void testDetectChange()
		throws Exception {

		// Override watch interval
		System.setProperty(CORSConfigurationFileWatcher.POLL_INTERVAL_PARAM_NAME, "1");

		MockFilterConfig filterConfig = new MockFilterConfig();
		filterConfig.setInitParameter(CORSConfigurationLoader.CONFIG_FILE_PARAM_NAME, "test.properties");

		CORSConfigurationFileWatcher watcher = new CORSConfigurationFileWatcher(filterConfig);

		assertEquals(1L, watcher.getPollIntervalSeconds());

		watcher.start();

		Properties properties = new Properties();
		properties.setProperty("cors.allowOrigin", "https://www.example.com");

		OutputStream os = Files.newOutputStream(new File("test.properties").toPath());
		properties.store(os, null);

		Thread.sleep(1100);

		assertTrue(watcher.reloadRequired());

		watcher.stop();
	}


	@Test
	public void testDetectNoChange()
		throws Exception {

		// Override watch interval
		System.setProperty(CORSConfigurationFileWatcher.POLL_INTERVAL_PARAM_NAME, "1");

		MockFilterConfig filterConfig = new MockFilterConfig();
		filterConfig.setInitParameter(CORSConfigurationLoader.CONFIG_FILE_PARAM_NAME, "test.properties");

		CORSConfigurationFileWatcher watcher = new CORSConfigurationFileWatcher(filterConfig);

		assertEquals(1L, watcher.getPollIntervalSeconds());

		watcher.start();

		Thread.sleep(1100);

		assertFalse(watcher.reloadRequired());

		watcher.stop();
	}


	@AfterEach
	public void tearDown() {

		System.clearProperty("cors.configFilePollInterval");

		new File("test.properties").delete();
	}
}
